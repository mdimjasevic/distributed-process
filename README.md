# Introduction

This repository implements the CH/OTP Test Task via the distributed-process
library. It uses a master-slave model where master collects and distributes a
list of slaves, where each slave corresponds to a node. Nodes communicate
asynchronously. The master and slaves are each started on a host and port
specified on the command line.

# Building the project

Project can be built by running `stack build`.

# Running

To run the task, start slave processes first. For example, to start two slaves
on the localhost, run in two separate consoles:

  stack runghc -- app/Main.hs --wait-for 1 --send-for 2 --with-seed 5 --mode slave --host localhost --port 10532
  stack runghc -- app/Main.hs --wait-for 1 --send-for 2 --with-seed 5 --mode slave --host localhost --port 10535

Then start the master process in a new console, e.g., on the localhost:

  stack runghc -- app/Main.hs --wait-for 1 --send-for 1 --with-seed 1 --mode Master --host localhost --port 11013
