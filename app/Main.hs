{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-cse #-}

module Main where

import Control.Concurrent (threadDelay)
import Control.Distributed.Process
import Control.Distributed.Process.Closure
import Control.Distributed.Process.Backend.SimpleLocalnet
import Control.Distributed.Process.Node hiding (newLocalNode)
import Control.Monad (forM_, forM)
import Data.Binary
import Data.Function
import Data.List
import Data.Maybe (catMaybes)
import Data.Time.Clock.POSIX
import GHC.Generics
import System.Console.CmdArgs hiding (Mode)
import GHC.Exts (build)
import System.IO
import System.Random

data Mode = Master | Slave deriving (Show, Read, Eq, Data)

newtype SendFor  = MkSendFor  Int deriving (Show, Read, Eq, Data, Generic, Typeable)
newtype WaitFor  = MkWaitFor  Int deriving (Show, Read, Eq, Data, Generic, Typeable)
newtype WithSeed = MkWithSeed Int deriving (Show, Read, Eq, Data)

instance Binary SendFor
instance Binary WaitFor


data Dist = MkDist
  {
    send_for :: SendFor
  , wait_for :: WaitFor
  , with_seed :: WithSeed
  , host :: String
  , port :: String
  , mode :: Mode
  } deriving (Show, Data, Typeable)

-- Command line arguments
allArgs = MkDist
  {
    send_for = MkSendFor 5    &= help "Seconds for sending messages"
  , wait_for = MkWaitFor 2    &= help "Seconds for waiting for messages"
  , with_seed = MkWithSeed 1  &= help "The seed for random numbers"
  , host = "localhost"        &= help "The host to run this on"
  , port = def                &= help "The port to run this on"
  , mode = Master             &= help "The master or slave mode for this node"
  }



-- Generate a random number in the interval (0, 1]
customRandom :: RandomGen g => Fractional a => Random a => g -> (a, g)
customRandom g = let (v, g') = random g in (1 - v, g')

-- Adopted from System.Random (randoms)
customRandoms :: RandomGen g => Fractional a => Random a => g -> [a]
customRandoms g = build (\cons _nil -> buildRandoms cons customRandom g)

-- Taken from System.Random
buildRandoms :: RandomGen g
             => (a -> as -> as)  -- ^ E.g. '(:)' but subject to fusion
             -> (g -> (a,g))     -- ^ E.g. 'random'
             -> g                -- ^ A 'RandomGen' instance
             -> as
buildRandoms cons rand = go
  where
    -- The seq fixes part of #4218 and also makes fused Core simpler.
    go g = x `seq` (x `cons` go g') where (x,g') = rand g


-- Describes actions of a node parameterised by a seed and time limits for sending and waiting
task :: (Int, SendFor, WaitFor) -> Process ()
task (g, MkSendFor sf, MkWaitFor wf) = do
  -- receive a list of peers
  peerPids <- expect :: Process [ProcessId]
  let ns = take (length peerPids) (customRandoms (mkStdGen g) :: [Double])
  -- sends messages that contain a pair of time and a random number
  forM_ (zip peerPids ns) $ \(p, n) -> do
    t <- liftIO getPOSIXTime
    let t' = (read . reverse . drop 1 . reverse . show) t :: Double -- just to be able to serialize a timestamp
    send p (t', n) -- an asynchronous send that always succeeds
  -- receive messages from other peers
  recvds <- forM peerPids $ const (expectTimeout ((sf + wf) * 1000000) :: Process (Maybe (Double, Double)))
  say $ "Received these messages: " ++ show recvds
  let filtered = catMaybes recvds
  let sorted = sortBy (compare `on` fst) filtered -- sort by send time
  let paired = (snd <$> sorted) `zip` (fromInteger <$> [1..] :: [Double])
  let totalSum = foldr (\p acc -> acc + uncurry (*) p) 0.0 paired
  say $ "Total sum: " ++ show totalSum

-- The remote table
remotable ['task]

rtable :: RemoteTable
rtable = Main.__remoteTable initRemoteTable


-- Starts a master and slave processes
master :: Backend -> SendFor -> WaitFor -> WithSeed -> IO ()
master backend sf@(MkSendFor t) wf@(MkWaitFor w) (MkWithSeed s) = startMaster backend (runSlaves backend) where
  runSlaves :: Backend -> [NodeId] -> Process ()
  runSlaves backend slaves = do
    -- Generate seeds for slaves
    let wss = take (length slaves) (randoms (mkStdGen s) :: [Int])
    -- Do something interesting with the slaves
    liftIO . hPutStrLn stderr $ "Slaves: " ++ show slaves
    us <- getSelfNode
    -- spawn remote processes
    pids <- forM wss $ \g -> spawn us $ $(mkClosure 'task) (g, sf, wf)
    -- send each process the list of other
    forM_ pids $ \p -> send p pids
    liftIO $ threadDelay ((t + w) * 1000000)
    -- Terminate the slaves when the master terminates
    terminateAllSlaves backend


main :: IO ()
main = do
  args <- cmdArgs allArgs
  case mode args of
    Master -> do
      backend <- initializeBackend (host args) (port args) rtable
      master backend (send_for args) (wait_for args) (with_seed args)
    Slave -> do
      backend <- initializeBackend (host args) (port args) rtable
      startSlave backend
